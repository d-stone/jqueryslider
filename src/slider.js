/**
*   @file jQuery Image Slider Plugin
*   @description Wrote a simple image slider plugin. People can hook in specific events (before_slide, after_slide, etc)
*   see settings for 
*/

(function(window, $) {
    
    $.slider = function(object, settings) {
        var self = this;

        this.globals = {
            current_index: 0,
            slide_count: 0,
            slide_height: 0,
            slide_width: 0,
            interval: undefined,
            timeout: undefined,
            double: undefined,
            autoslide: false,
            lock: false,
        };

        // allow users to hook in specific events:
        // feel free to extend by simply calling self.hook('setting_function').
        // the hook name MUST be a property of the settings.hooks object and it MUST be a function
        this.hook = function(function_name) {
            if(settings.hooks.hasOwnProperty(function_name) && typeof settings.hooks[function_name] == 'function') {
                settings.hooks[function_name]();
            }
        }

        this.slide = function(prev_or_next) {
            self.globals.lock = true;

            self.hook('before_slide');

            // reset autoslide
            clearInterval(self.globals.interval);
            self.handleAutoslide();
                
            // setup new index:
            if(prev_or_next == 'prev') {
                self.globals.current_index -= 1;
                if(self.globals.current_index < 0) {
                    self.globals.current_index = self.globals.slide_count - 1;
                }
            } else {
                self.globals.current_index += 1;
                if(self.globals.current_index > (self.globals.slide_count - 1)) {
                    self.globals.current_index = 0;
                }
            }

            // act like like defined in settings
            switch(settings.direction) {                
                case 'horizontal':
                    // needs work if wanted to be animated
                    self.setupStyle();
                break;
                case 'vertical':
                default:
                    var removeDouble = false;

                    // no looping version:
                    if(settings.mode == undefined || settings.mode == 'default') {
                         // add the first item to the end, when 2 slides are shown
                        if(settings.show_slides > 1 && self.globals.current_index == (self.globals.slide_count - 1)) {
                            self.globals.double = settings.elements.inner.find('> div:first-child').clone();
                            settings.elements.inner.append(self.globals.double);
                        } else {
                            removeDouble = true;
                        }
                        settings.elements.inner.stop().animate({
                            bottom: (self.globals.slide_height * self.globals.current_index)
                        }, settings.slide_speed, function() {
                            if(self.globals.double !== undefined && removeDouble) {
                                self.globals.double.remove();
                            }
                        });
                    } else if (settings.mode == 'loop') {
                        value = prev_or_next == 'next' ? self.globals.slide_height : 0;

                        last = settings.elements.inner.find('> div:last');
                        first = settings.elements.inner.find('> div:first');

                        if(prev_or_next == 'prev') {
                            first.before(last);
                            settings.elements.inner.scrollTop(self.globals.slide_height);
                        }

                        settings.elements.inner.stop().animate({scrollTop: value}, {
                            duration: settings.slide_speed,
                            done: function() {
                                if(prev_or_next == 'next') {
                                    last.after(first);
                                    self.globals.lock = false;
                                }
                            }
                        });
                        
                    }
                break;
            }

            self.hook('after_slide');
        }

        // Initialisation
        this.setupGlobals = function() {
            $.extend(settings, {
                elements: {
                    container: object.find(settings.selectors.slider_container),
                    inner: object.find(settings.selectors.slider_inner),
                    prev: object.find(settings.selectors.prev),
                    next: object.find(settings.selectors.next),
                }
            });

            self.globals.current_index = settings.start_index !== undefined ? settings.start_index : 0;
            self.globals.slide_count = settings.elements.inner.children().length;
            self.globals.slide_height = settings.elements.inner.height();
            self.globals.slide_width = settings.elements.inner.width();
            self.globals.autoslide = settings.autoslide;
        }
        this.setupStyle = function() {
            switch(settings.direction) {
                case 'horizontal':
                    // set slider-width to single-slide width:
                    settings.elements.container.width(self.globals.slide_width);
                    // reset slides:
                    settings.elements.inner.find('> div').css({
                        'position': 'absolute',
                        'top': 0,
                        'left': 0,
                        'transform': 'none',
                    });

                    var i = self.globals.current_index + 1;
                    currentObject = settings.elements.inner.find('> div:nth-child(' + i + ')');

                    prevObject = i == 1 ? settings.elements.inner.find('> div:last-child') : currentObject.prev();
                    nextObject = i == (self.globals.slide_count) ? settings.elements.inner.find('> div:first-child') : currentObject.next();

                    prevObject.css('transform', 'translate3d(-' + self.globals.slide_width + 'px, 0px, 0px)');
                    nextObject.css('transform', 'translate3d(' + self.globals.slide_width + 'px, 0px, 0px)');
                break;
                case 'vertical':
                default:
                    // nothing to do in here
                    if(settings.show_slides > 1) {
                        settings.elements.container.height((settings.show_slides * self.globals.slide_height));
                    }
                    if(settings.mode == 'loop') {
                        settings.elements.inner.css({
                            height: settings.show_slides * self.globals.slide_height,
                            position: 'relative',
                            overflow: 'hidden',
                        });
                    }
                break;
            }
        }
        this.handleClicks = function() {
            settings.elements.prev.click(function(e) {
                self.slide('prev');
            });
            settings.elements.next.click(function(e) {
                self.slide('next');
            });
        }
        this.handleAutoslide = function() {
            direction = 'next';

            if(self.globals.autoslide) {
                if(settings.hasOwnProperty('autoslide_direction') && settings.autoslide_direction != 'normal') {
                    direction = 'prev';
                }

                self.globals.interval = setInterval(function() {
                    self.hook('before_autoslide');
                    self.slide(direction);
                    self.hook('after_autoslide');
                }, settings.autoslide_interval);
            }
        }
        this.pauseAutoslide = function() {
            self.globals.autoslide = false;
            clearInterval(self.globals.interval);
            return "paused";
        }
        this.startAutoslide = function() {
            self.globals.autoslide = true;
            self.handleAutoslide();
            return "started";
        }
        this.init = function() {
            self.hook('before_init');

            self.setupGlobals();
            self.setupStyle();
            self.handleClicks();
            self.handleAutoslide();

            self.hook('after_init');

            return object;
        }
    };
    $.fn.slider = function(settings) {
        return this.each(function() {
            object = $(this);

            // The settings can be overwritten on slider-call: .slider(settings))
            settings = $.extend({
                direction: 'vertical',  // vertical is the only settings which works correctly at the moment; there are cases in the script, which work on horizontal, but they're not finished yet.
                autoslide: true,
                autoslide_direction: 'normal',
                autoslide_interval: 5000,
                slide_speed: 300,
                show_slides: 3,
                hooks: {
                    before_slide: function() {},    // fires before the slide-event starts
                    after_slide: function() {},     // fires after the slide-event finished
                    before_autoslide: function() {},    // fires only before auto-slides (not on button-clicks)
                    after_autoslide: function() {},     // fires only when auto-slides finished (slides after button-click)
                    before_init: function() {},     // fires before anything else is done by the script
                    after_init: function() {},      // fires after the script has initialized completely.
                },
                selectors: {
                    slider_container: '#slider_container',
                    slider_inner: '#slider_inner',
                    prev: '#prev',
                    next: '#next',
                }
            }, settings);

            slider = new $.slider(object, settings);
            slider.init();
        });
    };
    
}(window, jQuery));