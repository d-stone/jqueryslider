(function(window, $) {
    $(document).ready(function() {
        /* default settings:

        defaultSettings = {
            direction: 'vertical',  // vertical is the only settings which works correctly at the moment; there are cases in the script, which work on horizontal, but they're not finished yet.
            autoslide: true,
            autoslide_direction: 'normal',
            autoslide_interval: 5000,
            slide_speed: 500,
            show_slides: 1,
            hooks: {
                before_slide: function() {},    // fires before the slide-event starts
                after_slide: function() {},     // fires after the slide-event finished
                before_autoslide: function() {},    // fires only before auto-slides (not on button-clicks)
                after_autoslide: function() {},     // fires only when auto-slides finished (slides after button-click)
                before_init: function() {},     // fires before anything else is done by the script
                after_init: function() {},      // fires after the script has initialized completely.
            },
            selectors: {
                slider_container: '#slider_container',
                slider_inner: '#slider_inner',
                prev: '#prev',
                next: '#next',
            }
        };*/

        // set up selector settings
        var settings = {
            mode: 'loop',
            selectors: {
                slider_container: '#sliderboxes',
                slider_inner: '#sliderboxes-inner',
                prev: '.slidernav .arrow_up',
                next: '.slidernav .arrow_down',
            }
        };
        $('.slider').slider(settings);

        // to pause autoslide the slider:
        //slider.pauseAutoslide();

        // to start again:
        //slider.startAutoslide();
    });
}(window, jQuery));