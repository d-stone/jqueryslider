# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A very simple imageslider. Allows user hooks at specific events (before_slide, after_slide, ...)
* Easy to configure, easy to use
* Version 1.0

### How do I get set up? ###	
Simply setup the selector-settings:
* settings.selectors.slider_container: container
* settings.selectors.slider_inner: inner-container
* settings.selectors.prev: prev-button
* settings.selectors.next: next-button
After that, you can simply call:
    $('.slider-selector').slider(settings);
to initialize the slider.